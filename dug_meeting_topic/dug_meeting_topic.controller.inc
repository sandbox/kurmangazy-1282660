<?php

/**
 * @file
 * Controller class definition file for the example "DUG meeting topic" entity.
 */

/**
 * DUG Meeting Topic Controller
 */
class DugMeetingTopicController extends DrupalDefaultEntityController {
  public function __construct($entityType) {
    parent::__construct('dug_meeting_topic');
  }

  // The default controller doesn't provide a create() method, so add one.
  // This is a useful place to assign defaults to the properties.
  public function create() {
    $topic = new StdClass();
    $topic->date = format_date(REQUEST_TIME, 'custom', 'Y-m-d H:i:s');
    $topic->topic = '';
    $topic->speaker = '';
    return $topic;
  }

  // The default controller doesn't provide a save() method, so add one.
  public function save($topic) {
    if(empty($topic->tid)) {
      drupal_write_record('dug_meeting_topic', $topic);

      // Make sure to invoke the insert hook.
      module_invoke_all('entity_insert', $topic, 'dug_meeting_topic');
    }
    else {
      drupal_write_record('dug_meeting_topic', $topic, 'tid');

      // Make sure to invoke the update hook.
      module_invoke_all('entity_update', $topic, 'dug_meeting_topic');
    }

    return $topic;
  }

  // The default controller doesn't provide a delete() method, so add one.
  public function delete($tids) {
    if(empty($tids)) {
      return FALSE;
    }

    $topics = entity_load('dug_meeting_topic', $tids);
    db_delete('dug_meeting_topic')
      ->condition('tid', $tids, 'IN')
      ->execute();

    // Make sure to invoke the delete hook for each topic.
    foreach($topics as $topic) {
      module_invoke_all('entity_delete', $topic, 'dug_meeting_topic');
    }

    // It's necessary to clear the caches when an entity is deleted.
    cache_clear_all();
    $this->resetCache();
    return TRUE;
  }
}
